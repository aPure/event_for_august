if(navigator.userAgent.match(/Android/i)){
  document.head.insertAdjacentHTML( 'beforeEnd', '<link rel="stylesheet" href="css/framework7.material.min.css">');
  document.head.insertAdjacentHTML( 'beforeEnd', '<link rel="stylesheet" href="css/framework7.material.colors.min.css">');
}else if(navigator.userAgent.match(/iPhone|iPad|iPod/i)){
  document.head.insertAdjacentHTML( 'beforeEnd', '<link rel="stylesheet" href="css/framework7.ios.min.css">');
  document.head.insertAdjacentHTML( 'beforeEnd', '<link rel="stylesheet" href="css/framework7.ios.colors.min.css">');
}else{
  document.body.innerHTML = "No laptop here. Use a smartphone ... ";
  document.body.style.textAlign = "center";
  document.body.style.fontSize = "200%";
}

var myApp = new Framework7();
var $$ = Dom7;

var mainView = myApp.addView('.view-main', {
  // Because we want to use dynamic navbar, we need to enable it for this view:
  //dynamicNavbar: true
  domCache: true
});

var storeDatas = myApp.formGetData('store');

//if(!storeDatas || storeDatas.store_name == ""){
mainView.router.load({pageName: 'index'});
//}
// else{
//   console.log(storeDatas.store_name + match_store_number(storeDatas.store_name));
//   $$('.qr_code').show();
//   $$('.next_button').show();
//   $$('.bar_code').show();
//   $$('.good_luck_button').show();
//   $$('.qr_code').attr('src', 'img/qrcode/'+match_store_number(storeDatas.store_name)+'.jpg');
//   $$('.bar_code').attr('src', 'img/barcode/'+match_store_number(storeDatas.store_name)+'barcode.jpg');
// }
 $$('.next_button').on('click',function(){
   myApp.alert("請期待下次的活動", "本活動已於9/15結束");
 });

// $$('.set_store').on('click', function(){
//   var storeName = myApp.formToData('#store');
//   if(storeName.store_name == ""){
//     $$('.store_name_message p').text("門市必要");
//     $$('.store_name_message p').css({color: 'red'});
//   }else{
//     var storedData = myApp.formStoreData('store', storeName);
//     mainView.router.load({pageName: 'qrcode'});
//     window.location.reload(false);
//   }
// });

$$('.form-to-data').on('click', function(){
  var formData = myApp.formToData('#identity');
  // If name or celphone is empty
  if(formData.name == "" || formData.cel == ""){
    $$('.error-message').show();
    $$('.error-message p').text("請輸入你的姓名與電話喔");
  }else{
    //console.log(formData);
    //13.113.64.137
    myApp.confirm(formData.name +" 和 "+ formData.cel+" ?", "請確認一下你的資料", function(){

      var data = {name: formData.name, cel: formData.cel, store: storeDatas.store_name};
      var api_url = 'http://13.115.204.182/index.php?action/event/';
      $$.get(api_url, data, function(data, status, xhr){
        var resp = JSON.parse(xhr.response);
        console.log(resp);
        $$('#user_code').text(resp.code);
        $$('.received_code').text(resp.valid_phone);
        //$$('.user_exist').text(resp.check);
        if(resp.check == 1){
          myApp.alert("你不能參加這一次的活動", "很抱歉");
        }else if(resp.check == 2){
          myApp.alert("不能兩次", "很抱歉");
        }else{
          mainView.router.load({pageName: 'phone_validate'});
        }
      });

    });
    

  }
});

$$('.phone_code').on('click', function(){
  var entered_code = myApp.formToData('#phone_validate');
  var received_from_server = $$('.received_code').text();
  console.log("entered: "+entered_code.phone_code, "received: "+received_from_server)
  if(received_from_server == entered_code.phone_code){
    mainView.router.load({pageName: 'code'});
  }else{
    $$('.validate_phone_message p').text("您的確認碼打錯囉，請重新在輸入一次");
    $$('.validate_phone_message p').css({color: 'red'});
  }
});

$$('.good_luck_button').on('click', function(){
  var deletion = myApp.formDeleteData('phone_validate');
  myApp.confirm($$('#user_code').text(), "不要忘記抄下客人的領取序號", function(){
    window.location.reload(false);
  });
  //window.location.reload(false);
});

function match_store_number(store){
  //console.log("akd");
  switch(store){
    case '精明門市':
      return '15001';
      break;
    case '信義門市':
      return '15002';
      break;
    case '頂溪門市':
      return '15014';
      break;
    case '新光站前':
      return '16001';
      break;
    case '新光信義A11':
      return '16002';
      break;
    case '台中新光三越':
      return '16003';
      break;
    case '桃園遠百':
      return '16007';
      break;
    case '高雄大遠百':
      return '16011';
      break;
    case '漢神巨蛋':
      return '16016';
      break;
    case '台中中友':
      return '16017';
      break;
    case '新光台南西門':
      return '16024';
      break;
    case '中壢SOGO':
      return '16028';
      break;
    case '台中大遠百':
      return '16029';
      break;
    case '板橋中山遠百':
      return '16032';
      break;
    case '高雄新光左營':
      return '16033';
      break;
    case '新竹巨城':
      return '16037';
      break;
    case '新光南西':
      return '16038';
      break;
    case '新光信義A8':
      return '16045';
      break;
    case '高雄夢時代':
      return '16048';
      break;
    case '復興SOGO':
      return '16049';
      break;
    case '大江購物中心':
      return '16050';
      break;
  }
}
